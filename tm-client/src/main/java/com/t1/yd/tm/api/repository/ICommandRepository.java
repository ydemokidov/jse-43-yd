package com.t1.yd.tm.api.repository;

import com.t1.yd.tm.command.AbstractCommand;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;

public interface ICommandRepository {

    void add(AbstractCommand command);

    @Nullable
    AbstractCommand getCommandByArgument(@NotNull String argument);

    @Nullable
    AbstractCommand getCommandByName(@NotNull String name);

    @NotNull
    Collection<AbstractCommand> getTerminalCommands();

    @NotNull
    Collection<AbstractCommand> getCommandsWithArgument();

}
