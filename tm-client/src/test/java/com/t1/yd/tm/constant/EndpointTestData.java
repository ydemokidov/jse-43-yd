package com.t1.yd.tm.constant;

import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;

@UtilityClass
public class EndpointTestData {

    @NotNull
    public static final String ADMIN_LOGIN = "admin";

    @NotNull
    public static final String ADMIN_PASSWORD = "admin";

    @NotNull
    public static final String USER1_LOGIN = "user1";

    @NotNull
    public static final String USER1_PASSWORD = "user1";

}
