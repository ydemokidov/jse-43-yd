package com.t1.yd.tm.service.model;

import com.t1.yd.tm.api.repository.model.ISessionRepository;
import com.t1.yd.tm.api.service.IConnectionService;
import com.t1.yd.tm.api.service.model.ISessionService;
import com.t1.yd.tm.model.Session;
import com.t1.yd.tm.repository.model.SessionRepository;
import org.jetbrains.annotations.NotNull;

import javax.persistence.EntityManager;

public class SessionService extends AbstractUserOwnedService<Session, ISessionRepository> implements ISessionService {

    public SessionService(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

    @NotNull
    @Override
    protected ISessionRepository getRepository(@NotNull final EntityManager entityManager) {
        return new SessionRepository(entityManager);
    }
}
