package com.t1.yd.tm.repository.model;

import com.t1.yd.tm.api.repository.model.IUserRepository;
import com.t1.yd.tm.model.User;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.EntityManager;

public class UserRepository extends AbstractRepository<User> implements IUserRepository {

    public UserRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public @Nullable User findByLogin(@NotNull final String login) {
        @NotNull final String query = String.format("FROM %s WHERE login=:login", getEntityName());
        return entityManager.createQuery(query, getClazz()).setParameter("login", login).getSingleResult();
    }

    @Override
    public @Nullable User findByEmail(@NotNull final String email) {
        @NotNull final String query = String.format("FROM %s WHERE email=:email", getEntityName());
        return entityManager.createQuery(query, getClazz()).setParameter("email", email).getSingleResult();
    }

    @Override
    protected @NotNull String getEntityName() {
        return getClazz().getSimpleName();
    }

    @Override
    protected @NotNull Class<User> getClazz() {
        return User.class;
    }
}
