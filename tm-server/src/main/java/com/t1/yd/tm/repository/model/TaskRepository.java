package com.t1.yd.tm.repository.model;

import com.t1.yd.tm.api.repository.model.ITaskRepository;
import com.t1.yd.tm.model.Task;
import org.jetbrains.annotations.NotNull;

import javax.persistence.EntityManager;
import java.util.List;


public class TaskRepository extends AbstractUserOwnedRepository<Task> implements ITaskRepository {

    public TaskRepository(@NotNull EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public @NotNull List<Task> findAllByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        @NotNull final String query = String.format("FROM %s WHERE project.id = :projectId", getEntityName());
        return entityManager.createQuery(query, getClazz()).setParameter("projectId", projectId).getResultList();
    }

    @Override
    protected @NotNull String getEntityName() {
        return getClazz().getSimpleName();
    }

    @Override
    protected @NotNull Class<Task> getClazz() {
        return Task.class;
    }

}
