package com.t1.yd.tm.service.dto;

import com.t1.yd.tm.api.repository.dto.IDtoProjectRepository;
import com.t1.yd.tm.api.service.IConnectionService;
import com.t1.yd.tm.api.service.dto.IProjectDtoService;
import com.t1.yd.tm.dto.model.ProjectDTO;
import com.t1.yd.tm.enumerated.Status;
import com.t1.yd.tm.exception.entity.ProjectNotFoundException;
import com.t1.yd.tm.exception.field.*;
import com.t1.yd.tm.repository.dto.ProjectDtoRepository;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.EntityManager;

public final class ProjectDtoService extends AbstractUserOwnedDtoService<ProjectDTO, IDtoProjectRepository> implements IProjectDtoService {

    public ProjectDtoService(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

    @NotNull
    @Override
    public ProjectDTO findProjectById(@NotNull final String id) {
        @Nullable final ProjectDTO projectDTO = findOneById(id);
        if (projectDTO == null) throw new ProjectNotFoundException();
        return projectDTO;
    }

    @NotNull
    public ProjectDTO findProjectByIndex(@NotNull final String userId, @NotNull final Integer index) {
        @Nullable final ProjectDTO projectDTO = findOneByIndex(userId, index);
        if (projectDTO == null) throw new ProjectNotFoundException();
        return projectDTO;
    }

    @NotNull
    @Override
    public ProjectDTO findProjectById(@NotNull final String userId, @NotNull final String id) {
        @Nullable final ProjectDTO projectDTO = findOneById(userId, id);
        if (projectDTO == null) throw new ProjectNotFoundException();
        return projectDTO;
    }

    @NotNull
    @Override
    public ProjectDTO create(@NotNull final String userId, @NotNull final String name, @NotNull final String description) {
        if (name.isEmpty()) throw new NameEmptyException();
        if (description.isEmpty()) throw new DescriptionEmptyException();
        return add(new ProjectDTO(userId, name, description));
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectDTO updateById(@NotNull final String userId, @NotNull final String id, @NotNull final String name, @NotNull final String description) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        if (id.isEmpty()) throw new IdEmptyException();
        if (name.isEmpty()) throw new NameEmptyException();

        @Nullable final ProjectDTO projectDTO = findOneById(userId, id);
        if (projectDTO == null) throw new ProjectNotFoundException();

        projectDTO.setName(name);
        projectDTO.setDescription(description);
        update(projectDTO);

        return projectDTO;
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectDTO updateByIndex(@NotNull final String userId, @NotNull final Integer index, @NotNull final String name, @NotNull final String description) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        if (index < 0) throw new IndexIncorrectException();
        if (name.isEmpty()) throw new NameEmptyException();

        @Nullable final ProjectDTO projectDTO = findOneByIndex(userId, index);
        if (projectDTO == null) throw new ProjectNotFoundException();

        projectDTO.setName(name);
        projectDTO.setDescription(description);
        update(projectDTO);

        return projectDTO;
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectDTO changeStatusById(@NotNull final String userId, @NotNull final String id, @NotNull final Status status) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        if (id.isEmpty()) throw new IdEmptyException();

        @Nullable final ProjectDTO projectDTO = findOneById(userId, id);
        if (projectDTO == null) throw new ProjectNotFoundException();

        projectDTO.setStatus(status);

        update(projectDTO);
        return projectDTO;
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectDTO changeStatusByIndex(@NotNull final String userId, @NotNull final Integer index, @NotNull final Status status) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        if (index < 0 || index > findAll(userId).size()) throw new IndexIncorrectException();
        @Nullable final ProjectDTO projectDTO = findOneByIndex(userId, index);
        if (projectDTO == null) throw new ProjectNotFoundException();

        projectDTO.setStatus(status);
        update(projectDTO);

        return projectDTO;
    }

    @NotNull
    @SneakyThrows
    public ProjectDTO removeProjectById(@NotNull final String userId, @NotNull final String id) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        if (id.isEmpty()) throw new IdEmptyException();

        @Nullable ProjectDTO projectDTO = removeById(userId, id);
        if (projectDTO == null) throw new ProjectNotFoundException();
        return projectDTO;
    }

    @NotNull
    @SneakyThrows
    public ProjectDTO removeProjectByIndex(@NotNull final String userId, @NotNull final Integer index) {
        @Nullable final ProjectDTO projectDTO = removeByIndex(userId, index);
        if (projectDTO == null) throw new ProjectNotFoundException();
        return projectDTO;
    }

    @NotNull
    @Override
    protected IDtoProjectRepository getRepository(@NotNull final EntityManager entityManager) {
        return new ProjectDtoRepository(entityManager);
    }

}