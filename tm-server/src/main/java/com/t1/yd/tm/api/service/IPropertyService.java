package com.t1.yd.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface IPropertyService extends ISaltProvider {

    @NotNull
    String getApplicationVersion();

    @NotNull
    String getAuthorName();

    @NotNull
    String getAuthorEmail();

    @NotNull String getDbSqlDialect();

    String getDbShowSqlFlg();

    @NotNull String getDbStrategy();

    @NotNull
    Integer getServerPort();

    @Nullable
    String getSessionKey();

    @NotNull
    String getDbServer();

    @NotNull
    String getDbUsername();

    @NotNull
    String getDbPassword();

    @NotNull
    String getDbDriver();

    int getSessionTimeout();

}
