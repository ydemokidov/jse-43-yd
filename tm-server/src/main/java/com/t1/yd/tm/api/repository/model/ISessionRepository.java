package com.t1.yd.tm.api.repository.model;

import com.t1.yd.tm.model.Session;
import org.jetbrains.annotations.NotNull;

import java.util.List;

public interface ISessionRepository extends IUserOwnedRepository<Session> {

    @NotNull List<Session> findAll(@NotNull String sort);

}
