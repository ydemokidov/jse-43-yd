package com.t1.yd.tm.repository.dto;

import com.t1.yd.tm.api.repository.dto.IDtoTaskRepository;
import com.t1.yd.tm.dto.model.TaskDTO;
import org.jetbrains.annotations.NotNull;

import javax.persistence.EntityManager;
import java.util.List;

public class TaskDtoRepository extends AbstractDtoUserOwnedRepository<TaskDTO> implements IDtoTaskRepository {

    public TaskDtoRepository(@NotNull EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public @NotNull List<TaskDTO> findAllByProjectId(@NotNull final String userId, final @NotNull String projectId) {
        String query = String.format("FROM %s WHERE project_id = :project_id", getEntityName());
        return entityManager.createQuery(query, getClazz())
                .setParameter("project_id", projectId)
                .getResultList();
    }

    @Override
    protected @NotNull String getEntityName() {
        return getClazz().getSimpleName();
    }

    @Override
    protected @NotNull Class<TaskDTO> getClazz() {
        return TaskDTO.class;
    }
}
