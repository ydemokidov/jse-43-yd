package com.t1.yd.tm.api.service.dto;

import com.t1.yd.tm.dto.model.ProjectDTO;
import com.t1.yd.tm.enumerated.Status;
import org.jetbrains.annotations.NotNull;

public interface IProjectDtoService extends IUserOwnedDtoService<ProjectDTO> {

    void clear(@NotNull String userId);

    @NotNull
    ProjectDTO create(@NotNull String userId, @NotNull String name, @NotNull String description);

    @NotNull
    ProjectDTO updateById(@NotNull String userId, @NotNull String id, @NotNull String name, @NotNull String description);

    @NotNull
    ProjectDTO updateByIndex(@NotNull String userId, @NotNull Integer index, @NotNull String name, @NotNull String description);

    @NotNull
    ProjectDTO changeStatusById(@NotNull String userId, @NotNull String id, @NotNull Status status);

    @NotNull
    ProjectDTO changeStatusByIndex(@NotNull String userId, @NotNull Integer index, @NotNull Status status);

    @NotNull
    ProjectDTO findProjectById(@NotNull String id);

    @NotNull
    ProjectDTO findProjectById(@NotNull String userId, @NotNull String id);

    @NotNull
    ProjectDTO findProjectByIndex(@NotNull String userId, @NotNull Integer index);

    @NotNull
    ProjectDTO removeProjectById(@NotNull String userId, @NotNull String id);

    @NotNull
    ProjectDTO removeProjectByIndex(@NotNull String userId, @NotNull Integer index);

}
