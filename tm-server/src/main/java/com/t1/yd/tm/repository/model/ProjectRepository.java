package com.t1.yd.tm.repository.model;

import com.t1.yd.tm.api.repository.model.IProjectRepository;
import com.t1.yd.tm.model.Project;
import org.jetbrains.annotations.NotNull;

import javax.persistence.EntityManager;

public class ProjectRepository extends AbstractUserOwnedRepository<Project> implements IProjectRepository {

    public ProjectRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    protected @NotNull String getEntityName() {
        return getClazz().getSimpleName();
    }

    @Override
    protected @NotNull Class<Project> getClazz() {
        return Project.class;
    }

}
