package com.t1.yd.tm.endpoint;

import com.t1.yd.tm.api.endpoint.IAuthEndpoint;
import com.t1.yd.tm.api.service.IAuthService;
import com.t1.yd.tm.api.service.IServiceLocator;
import com.t1.yd.tm.dto.model.SessionDTO;
import com.t1.yd.tm.dto.model.UserDTO;
import com.t1.yd.tm.dto.request.user.UserGetProfileRequest;
import com.t1.yd.tm.dto.request.user.UserLoginRequest;
import com.t1.yd.tm.dto.request.user.UserLogoutRequest;
import com.t1.yd.tm.dto.response.user.UserGetProfileResponse;
import com.t1.yd.tm.dto.response.user.UserLoginResponse;
import com.t1.yd.tm.dto.response.user.UserLogoutResponse;
import com.t1.yd.tm.exception.entity.UserNotFoundException;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.jws.WebParam;
import javax.jws.WebService;

@NoArgsConstructor
@WebService(endpointInterface = "com.t1.yd.tm.api.endpoint.IAuthEndpoint")
public class AuthEndpoint extends AbstractEndpoint implements IAuthEndpoint {

    public AuthEndpoint(@NotNull IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    private IAuthService getAuthService() {
        return getServiceLocator().getAuthService();
    }

    @Override
    public UserLoginResponse login(@WebParam(name = REQUEST, partName = REQUEST) @NotNull final UserLoginRequest request) {
        @NotNull String token = getAuthService().login(request.getLogin(), request.getPassword());
        @NotNull final UserLoginResponse response = new UserLoginResponse();
        response.setToken(token);

        return response;
    }

    @Override
    public UserLogoutResponse logout(@WebParam(name = REQUEST, partName = REQUEST) @NotNull final UserLogoutRequest request) {
        @NotNull final SessionDTO sessionDTO = check(request);
        getAuthService().logout(sessionDTO);
        return new UserLogoutResponse();
    }

    @Override
    public UserGetProfileResponse getProfile(@WebParam(name = REQUEST, partName = REQUEST) @NotNull UserGetProfileRequest request) {
        @NotNull final SessionDTO sessionDTO = check(request);
        @NotNull final String userId = sessionDTO.getUserId();
        @Nullable final UserDTO userDTO = getServiceLocator().getUserService().findOneById(userId);
        if (userDTO == null) throw new UserNotFoundException();
        return new UserGetProfileResponse(userDTO);
    }

}
