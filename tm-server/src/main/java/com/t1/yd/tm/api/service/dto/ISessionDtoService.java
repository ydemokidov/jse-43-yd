package com.t1.yd.tm.api.service.dto;

import com.t1.yd.tm.dto.model.SessionDTO;

public interface ISessionDtoService extends IUserOwnedDtoService<SessionDTO> {
}
