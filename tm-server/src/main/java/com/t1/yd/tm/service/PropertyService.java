package com.t1.yd.tm.service;

import com.jcabi.manifests.Manifests;
import com.t1.yd.tm.api.service.IPropertyService;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Properties;

public class PropertyService implements IPropertyService {

    @NotNull
    public static final String FILE_NAME = "application.properties";

    @NotNull
    public static final String SERVER_PORT_KEY = "server.port";

    @NotNull
    public static final String SERVER_HOST_KEY = "server.host";

    @NotNull
    public static final String SERVER_PORT_DEFAULT = "6060";

    @NotNull
    public static final String SERVER_HOST_DEFAULT = "0.0.0.0";

    @NotNull
    public static final String SESSION_KEY = "session.key";

    @NotNull
    public static final String SESSION_TIMEOUT = "session.timeout";

    @NotNull
    static final String SERVER_PORT = "server.port";

    @NotNull
    private static final String VERSION_KEY = "version";

    @NotNull
    private static final String AUTHOR_NAME_KEY = "developer";

    @NotNull
    private static final String AUTHOR_EMAIL_KEY = "email";

    @NotNull
    private static final String PWD_ITERATION_DEFAULT = "128";

    @NotNull
    private static final String PWD_ITERATION_KEY = "password.iteration";

    @NotNull
    private static final String PWD_SECRET_DEFAULT = "default";

    @NotNull
    private static final String PWD_SECRET_KEY = "password.secret";

    @NotNull
    private static final String EMPTY_VALUE = "---";

    @NotNull
    private static final String DB_CONN_STRING = "database.server";

    @NotNull
    private static final String DB_USERNAME = "database.username";

    @NotNull
    private static final String DB_PASSWORD = "database.password";

    @NotNull
    private static final String DB_DRIVER = "database.driver";

    @NotNull
    private static final String DB_DIALECT = "database.dialect";

    @NotNull
    private static final String DB_SHOW_SQL = "database.showSql";

    @NotNull
    private static final String DB_STRATEGY = "database.strategy";


    @NotNull
    private final Properties properties = new Properties();

    @SneakyThrows
    public PropertyService() {
        properties.load(ClassLoader.getSystemResourceAsStream(FILE_NAME));
    }

    @Override
    public @NotNull String getApplicationVersion() {
        return Manifests.read(VERSION_KEY);
    }

    @Override
    public @NotNull String getAuthorName() {
        return Manifests.read(AUTHOR_NAME_KEY);
    }

    @Override
    public @NotNull String getAuthorEmail() {
        return Manifests.read(AUTHOR_EMAIL_KEY);
    }

    @Override
    public @NotNull Integer getPasswordIteration() {
        final String result = getStringValue(PWD_ITERATION_KEY, PWD_ITERATION_DEFAULT);
        return Integer.parseInt(result);
    }

    @NotNull
    @Override
    public String getDbServer() {
        return getStringValue(DB_CONN_STRING);
    }

    @NotNull
    @Override
    public String getDbUsername() {
        return getStringValue(DB_USERNAME);
    }

    @NotNull
    @Override
    public String getDbPassword() {
        return getStringValue(DB_PASSWORD);
    }

    @NotNull
    @Override
    public String getDbDriver() {
        return getStringValue(DB_DRIVER);
    }

    @NotNull
    @Override
    public String getDbSqlDialect() {
        return getStringValue(DB_DIALECT);
    }

    @NotNull
    @Override
    public String getDbShowSqlFlg() {
        return getStringValue(DB_SHOW_SQL);
    }

    @NotNull
    @Override
    public String getDbStrategy() {
        return getStringValue(DB_STRATEGY);
    }

    @Override
    public @NotNull String getPasswordSecret() {
        return properties.getProperty(PWD_SECRET_KEY, PWD_SECRET_DEFAULT);
    }

    @Override
    public @NotNull String getHost() {
        return getStringValue(SERVER_HOST_KEY, SERVER_HOST_DEFAULT);
    }

    @Override
    public @NotNull String getPort() {
        return getStringValue(SERVER_PORT_KEY, SERVER_PORT_DEFAULT);
    }

    @NotNull
    private String getEnvKey(@NotNull final String key) {
        return key.replace(".", "_").toUpperCase();
    }

    @NotNull
    private String getStringValue(@NotNull final String key, @NotNull final String defaultValue) {
        if (System.getProperties().contains(key)) return System.getProperty(key);
        @NotNull final String envKey = getEnvKey(key);
        if (System.getenv().containsKey(envKey)) return System.getenv(envKey);
        return properties.getProperty(key, defaultValue);
    }

    @NotNull
    private String getStringValue(@NotNull final String key) {
        return getStringValue(key, EMPTY_VALUE);
    }

    @NotNull
    @Override
    @SneakyThrows
    public Integer getServerPort() {
        return Integer.parseInt(getStringValue(SERVER_PORT));
    }

    @Override
    public @Nullable String getSessionKey() {
        return getStringValue(SESSION_KEY);
    }

    @Override
    public int getSessionTimeout() {
        return Integer.parseInt(getStringValue(SESSION_TIMEOUT, ""));
    }

}
