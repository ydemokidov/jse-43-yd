package com.t1.yd.tm.constant;

import com.t1.yd.tm.dto.model.UserDTO;
import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;

import java.util.Arrays;
import java.util.List;

@UtilityClass
public class UserTestData {

    @NotNull
    public static final UserDTO USER_DTO_1 = new UserDTO();

    @NotNull
    public static final String USER1_LOGIN = "user1";

    @NotNull
    public static final String USER1_EMAIL = "user1@mail.ru";

    @NotNull
    public static final UserDTO ADMIN = new UserDTO();

    @NotNull
    public static final String ADMIN_LOGIN = "admin";

    @NotNull
    public static final String ADMIN_EMAIL = "admin@mail.ru";

    @NotNull
    public static final List<UserDTO> ALL_USER_DTOS = Arrays.asList(USER_DTO_1, ADMIN);

    static {
        USER_DTO_1.setLogin(USER1_LOGIN);
        USER_DTO_1.setEmail(USER1_EMAIL);
        USER_DTO_1.setPasswordHash("1234");

        ADMIN.setLogin(ADMIN_LOGIN);
        ADMIN.setEmail(ADMIN_EMAIL);
        ADMIN.setPasswordHash("2345");
    }

}
