package com.t1.yd.tm.dto.response.task;

import com.t1.yd.tm.dto.model.TaskDTO;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;

@NoArgsConstructor
public class TaskCreateResponse extends AbstractTaskResponse {

    public TaskCreateResponse(@Nullable final TaskDTO taskDTO) {
        super(taskDTO);
    }

    public TaskCreateResponse(@Nullable final Throwable throwable) {
        super(throwable);
    }

}
