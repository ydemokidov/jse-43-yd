package com.t1.yd.tm.model;

import com.t1.yd.tm.enumerated.Role;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@Entity
@Table(name = "sessions")
@NoArgsConstructor
public class Session extends AbstractUserOwnedEntity {

    @NotNull
    @Column(nullable = false)
    private Date date = new Date();

    @Nullable
    @Column(length = 50)
    @Enumerated(EnumType.STRING)
    private Role role = null;

    public Session(@NotNull User user) {
        super(user);
    }

}