package com.t1.yd.tm.dto.response.task;

import com.t1.yd.tm.dto.response.AbstractResultResponse;
import org.jetbrains.annotations.Nullable;

public class TaskClearResponse extends AbstractResultResponse {

    public TaskClearResponse(@Nullable final Throwable throwable) {
        super(throwable);
    }

    public TaskClearResponse() {
    }

}
