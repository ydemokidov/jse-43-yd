package com.t1.yd.tm.dto.request;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AbstractUserRequestById extends AbstractUserRequest {

    private String id;

}
