package com.t1.yd.tm.dto.response.data;

import com.t1.yd.tm.dto.response.AbstractResultResponse;
import org.jetbrains.annotations.Nullable;

public class DataBinarySaveResponse extends AbstractResultResponse {

    public DataBinarySaveResponse(@Nullable final Throwable throwable) {
        super(throwable);
    }

    public DataBinarySaveResponse() {

    }
}
