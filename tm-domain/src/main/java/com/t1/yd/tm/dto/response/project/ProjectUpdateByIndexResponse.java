package com.t1.yd.tm.dto.response.project;

import com.t1.yd.tm.dto.model.ProjectDTO;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;

@NoArgsConstructor
public class ProjectUpdateByIndexResponse extends AbstractProjectResponse {

    public ProjectUpdateByIndexResponse(@Nullable final ProjectDTO projectDTO) {
        super(projectDTO);
    }

    public ProjectUpdateByIndexResponse(@Nullable final Throwable throwable) {
        super(throwable);
    }

}
