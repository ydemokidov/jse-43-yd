package com.t1.yd.tm.dto.response.project;

import com.t1.yd.tm.dto.model.ProjectDTO;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;

@NoArgsConstructor
public class ProjectStartByIdResponse extends AbstractProjectResponse {

    public ProjectStartByIdResponse(@Nullable final ProjectDTO projectDTO) {
        super(projectDTO);
    }

    public ProjectStartByIdResponse(@Nullable final Throwable throwable) {
        super(throwable);
    }

}
