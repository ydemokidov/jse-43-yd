package com.t1.yd.tm.dto.response.user;

import com.t1.yd.tm.dto.model.UserDTO;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;

@NoArgsConstructor
public class UserUnlockResponse extends AbstractUserResponse {

    public UserUnlockResponse(@Nullable final UserDTO userDTO) {
        super(userDTO);
    }

    public UserUnlockResponse(@Nullable final Throwable throwable) {
        super(throwable);
    }

}
