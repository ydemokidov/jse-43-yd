package com.t1.yd.tm.model;

import com.t1.yd.tm.api.model.IWBS;
import com.t1.yd.tm.enumerated.Status;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@Entity
@Table(name = "projects")
@NoArgsConstructor
public final class Project extends AbstractUserOwnedEntity implements IWBS {

    @NotNull
    @Column(length = 100, nullable = false)
    private String name = "";

    @NotNull
    @Column(length = 200, nullable = false)
    private String description = "";

    @NotNull
    @Column(length = 50, nullable = false)
    @Enumerated(EnumType.STRING)
    private Status status = Status.NOT_STARTED;

    @NotNull
    @Column(nullable = false)
    private Date created = new Date();

    @NotNull
    @OneToMany(mappedBy = "project")
    private List<Task> tasks = new ArrayList<>();

    public Project(@NotNull final User user, @NotNull final String name, @NotNull final String description) {
        super(user);
        this.name = name;
        this.description = description;
    }

    @NotNull
    @Override
    public String toString() {
        return name + " : " + description;
    }
}